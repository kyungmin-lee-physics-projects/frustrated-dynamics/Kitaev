using DrWatson
@quickactivate "Kitaev"

using Kitaev

using LatticeTools
using QuantumHamiltonian

using Arpack
using KrylovKit
using ArgParse
using CodecXz
using MsgPack
using Logging
using DataStructures

include("localgenperm.jl")

function compute(shape::AbstractMatrix{<:Integer},
    theta_list::AbstractVector{<:Real},
    tii_list::AbstractVector{<:Integer},
    pii_list::AbstractVector{<:Integer},
    pic_list::AbstractVector{<:Integer},
    nev::Integer,
    ncv::Integer=2*nev+10;
    force::Bool=false,
   )
    shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"
    honeycomb = make_honeycomb_lattice(shape)
    tsymbed = honeycomb.space_symmetry_embedding.normal
    psym = project(PointSymmetryDatabase.find("-3"), [1 0 0; 0 1 0])
    psymbed = embed(honeycomb.lattice, psym)

    nsites = sitecount(honeycomb.lattice.supercell)
    hs, spin_operator = QuantumHamiltonian.Toolkit.spin_system(sitecount(honeycomb.lattice.supercell), 1//2)
    
    function rotS(isite::Integer, vec::AbstractVector{<:Real})
        @assert length(vec) == 3
        return spin_operator(isite, :x)*vec[1] + spin_operator(isite, :y)*vec[2] + spin_operator(isite, :z)*vec[3]
    end
    function rotS(isite::Integer, c::Integer)
        rotmat = [  1/sqrt(6)   -1/sqrt(2)   1/sqrt(3);
                    1/sqrt(6)    1/sqrt(2)   1/sqrt(3);
                   -sqrt(2/3)    0           1/sqrt(3)]
        return rotS(isite, rotmat[c, :])
    end

    local_spin_rotation = Dict(
        "1" => LocalGenPerm([1=>one(ComplexF64), 2=>one(ComplexF64)]),
        "3<sup>+</sup><sub>001</sub>"  => LocalGenPerm([1=>cis(-π/3), 2=>cis( π/3)]),
        "3<sup>-</sup><sub>001</sub>"  => LocalGenPerm([1=>cis( π/3), 2=>cis(-π/3)]),
        "-1" => LocalGenPerm([1=>one(ComplexF64), 2=>one(ComplexF64)]),
        "-3<sup>+</sup><sub>001</sub>" => LocalGenPerm([1=>cis(-π/3), 2=>cis( π/3)]),
        "-3<sup>-</sup><sub>001</sub>" => LocalGenPerm([1=>cis( π/3), 2=>cis(-π/3)]),
    )


    let
        hsr = represent(hs)

        hamiltonian0 = simplify(sum(
            rotS(isite, bondtype) * rotS(jsite, bondtype)
            for ((isite, jsite), R, bondtype) in honeycomb.nearest_neighbor_bonds
        ))

        field0 = simplify(sum(
            spin_operator(isite, :z)
            for isite in 1:sitecount(honeycomb.lattice.supercell)
        ))

        # ssymbed = honeycomb.space_symmetry_embedding
        @mylogmsg "Hilbert space dimension: $(dimension(hsr))"
        for tii in 1:num_irreps(tsymbed)
            !isempty(tii_list) && tii ∉ tii_list && continue

            tsymop_and_amplitudes = [
                let
                    (local_spin_rotation["1"] ×ˢ el, ampl)
                end
                for (el, ampl) in get_irrep_iterator(IrrepComponent(tsymbed, tii))
            ]

            psymbed_little = little_symmetry(tsymbed, tii, psymbed)

            for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
                !isempty(pii_list) && pii ∉ pii_list && continue
                !isempty(pic_list) && pic ∉ pic_list && continue

                psymop_and_amplitudes = [
                    let
                        (local_spin_rotation[elname] ×ˢ el, ampl)
                    end
                    for ((el, ampl), elname) in zip(get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic)), element_names(psymbed_little))
                ]
                @mylogmsg "space symmetry sector: ($tii, $pii, $pic)"

                symop_and_amplitudes = make_product_irrep(tsymop_and_amplitudes, psymop_and_amplitudes)

                rhsr = symmetry_reduce(hsr, symop_and_amplitudes)
                @mylogmsg "reduced Hilbert space dimension: $(dimension(rhsr))"
                for theta in theta_list
                    parameters = Dict("shape"=>shape_str, "theta"=>theta, "tii"=>tii, "pii"=>pii, "pic"=>pic)
                    output_filename = savename("spectrum", parameters, "msgpack.xz")
                    output_filepath = datadir("spectrum-sg", shape_str, output_filename)

                    if ispath(output_filepath)
                        if force
                            @mylogmsg "File $output_filepath exists. Overwriting."
                        else
                            @mylogmsg "File $output_filepath exists. Skipping."
                            continue
                        end
                    end
                    @mylogmsg "Will save to $output_filepath"
                    @mylogmsg "theta: $theta"

                    hamiltonian = hamiltonian0 * cos(theta * π/2) - field0 * sin(theta * π/2)

                    @mylogmsg "isinvariant: $(all(isinvariant(hs, x, hamiltonian) for (x,y) in tsymop_and_amplitudes))"

                    hamiltonian_rep = represent(rhsr, hamiltonian)
                    @mylogmsg "diagonalizing Hamiltonian of size $(size(hamiltonian_rep))"
                    
                    eigenvalues, eigenvectors, info = eigsolve(
                        x -> hamiltonian_rep * x,
                        dimension(rhsr),
                        min(nev, dimension(rhsr)),
                        :SR;
                        krylovdim=min(max(2*nev+1, ncv), dimension(rhsr)),
                        ishermitian=true,
                        verbosity=2
                    )

                    info = OrderedDict(
                        "converged" => info.converged,
                        "normres" => info.normres,
                        "numops" => info.numops,
                        "numiter" => info.numiter,
                    )
                    save_data = OrderedDict(
                        "parameter" => OrderedDict(
                            "theta" => theta,
                            "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]]
                        ),
                        "sector" => OrderedDict(
                            "tii" => tii,
                            "pii" => pii,
                            "pic" => pic,
                            "dimension" => dimension(rhsr),
                        ),
                        "eigenvalue" => eigenvalues,
                        "info" => info,
                    )

                    @mylogmsg "# Saving to $output_filepath"
                    let output_directory = dirname(output_filepath)
                        if !isdir(output_directory)
                            mkpath(output_directory)
                        end
                    end
                    open(output_filepath, "w") do io
                        ioc = XzCompressorStream(io)
                        MsgPack.pack(ioc, save_data)
                        close(ioc)
                    end
                end # for theta
            end # for pii, pic
        end # for tii
    end
end



function parse_commandline()
    s = ArgParse.ArgParseSettings()
    ArgParse.@add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--theta"
            arg_type = Float64
            nargs = '*'
            help = "values of theta"
        "--tii"
            arg_type = Int
            nargs = '*'
            help = "tii"
        "--pii"
            arg_type = Int
            nargs = '*'
            help = "pii"
        "--pic"
            arg_type = Int
            nargs = '*'
            help = "pic"
        "--nev"
            arg_type = Int
            default = 100
        "--ncv"
            arg_type = Int
            default = -1
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
        "--debug", "-d"
            help = "debug"
            action = :store_true
    end
    return ArgParse.parse_args(s)
end

function main()

    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])
    theta_list = parsed_args["theta"]
    tii_list = parsed_args["tii"]
    pii_list = parsed_args["pii"]
    pic_list = parsed_args["pic"]
    nev = parsed_args["nev"]
    ncv = parsed_args["ncv"]
    force = parsed_args["force"]

    if ncv <= nev
      ncv = 2*nev + 10
    end

    compute(shape, theta_list, tii_list,
        pii_list, pic_list,
        nev, ncv
    ; force=force)
end

main()
