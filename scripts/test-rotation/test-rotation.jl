using DrWatson
@quickactivate "Kitaev"

using Kitaev

using LatticeTools
using QuantumHamiltonian

using PyPlot

using Arpack
using KrylovKit
using DataFrames
using LinearAlgebra

include("localgenperm.jl")

# shape = [2 0; 0 2]
shape = [3 0; 0 3]

honeycomb = make_honeycomb_lattice(shape)
nsites = sitecount(honeycomb.lattice.supercell)
hs, spin_operator = QuantumHamiltonian.Toolkit.spin_system(sitecount(honeycomb.lattice.supercell), 1//2)
_, pauli = QuantumHamiltonian.Toolkit.spin_half_system(sitecount(honeycomb.lattice.supercell))

function rotS(isite::Integer, vec::AbstractVector{<:Real})
    @assert length(vec) == 3
    return spin_operator(isite, :x) * vec[1] + spin_operator(isite, :y) * vec[2] + spin_operator(isite, :z) * vec[3]
end

function rotS(isite::Integer, c::Integer)
    rotmat = [ 1/sqrt(6)  -1/sqrt(2)   1/sqrt(3);
               1/sqrt(6)   1/sqrt(2)   1/sqrt(3);
              -2/sqrt(6)   0           1/sqrt(3)]
    return simplify(rotS(isite, rotmat[c, :]))
end

K = [1, 1, 1]

kitaev0 = simplify(sum(
    rotS(isite, bondtype) * rotS(jsite, bondtype) * K[bondtype]
    for ((isite, jsite), R, bondtype) in honeycomb.nearest_neighbor_bonds
))


kitaev0p = simplify(sum(
    rotS(isite, [2,1,3][bondtype]) * rotS(jsite, [2,1,3][bondtype]) * K[bondtype]
    for ((isite, jsite), R, bondtype) in honeycomb.nearest_neighbor_bonds
))

xy0 = simplify(sum(
    spin_operator(isite, bondtype) * spin_operator(jsite, bondtype)
    for ((isite, jsite), R, _) in honeycomb.nearest_neighbor_bonds
        for bondtype in [:x, :y]
))

x0 = simplify(sum(
    spin_operator(isite, bondtype) * spin_operator(jsite, bondtype)
    for ((isite, jsite), R, _) in honeycomb.nearest_neighbor_bonds
        for bondtype in [:x,]
))

heisenberg0 = simplify(sum(
    rotS(isite, bondtype) * rotS(jsite, bondtype)
    for ((isite, jsite), R, _) in honeycomb.nearest_neighbor_bonds
        for bondtype in [1,2,3]
))

field0 = simplify(sum(
    spin_operator(isite, :z)
    for isite in 1:sitecount(honeycomb.lattice.supercell)
))

ssymbed = honeycomb.space_symmetry_embedding
tsymbed = ssymbed.normal
psymbed = ssymbed.rest

psym = project(PointSymmetryDatabase.find3d("-3"), [1 0 0; 0 1 0])
psymbed = embed(honeycomb.lattice, psym)

@assert isinvariant(hs, tsymbed, kitaev0)
@assert isinvariant(hs, tsymbed, xy0)
@assert isinvariant(hs, tsymbed, field0)

spin_op = Dict(
    "1" => LocalGenPerm([1=>one(ComplexF64), 2=>one(ComplexF64)]),
    "3<sup>+</sup><sub>001</sub>"  => LocalGenPerm([1=>cis(-π/3), 2=>cis( π/3)]),
    "3<sup>-</sup><sub>001</sub>"  => LocalGenPerm([1=>cis( π/3), 2=>cis(-π/3)]),
    "-1" => LocalGenPerm([1=>one(ComplexF64), 2=>one(ComplexF64)]),
    "-3<sup>+</sup><sub>001</sub>" => LocalGenPerm([1=>cis(-π/3), 2=>cis( π/3)]),
    "-3<sup>-</sup><sub>001</sub>" => LocalGenPerm([1=>cis( π/3), 2=>cis(-π/3)]),
)

new_elements = [
    let
        els = spin_op[elname]
        e2 = els ×ˢ el
    end
    for (el, elname) in zip(psymbed.elements, psym.element_names)
]

# function QuantumHamiltonian.symmetry_apply(
#     hs::HilbertSpace,
#     symop::AbstractSymmetryOperation,
#     op::SumOperator
# )
#     terms = collect(symmetry_apply(hs, symop, t) for t in op.terms)
#     return SumOperator(terms)
# end

hsr = represent(hs)
@show dimension(hsr)

hamiltonian = cos(0.3) * kitaev0 - sin(0.3) * field0

evt = Float64[]
evp = Float64[]
evtp = Float64[]

totaldim = 0
for tii in 1:num_irreps(ssymbed.normal)
    tsymop_and_amplitudes = [
        let
            (spin_op["1"] ×ˢ el, ampl)
        end
        for (el, ampl) in get_irrep_iterator(IrrepComponent(ssymbed.normal, tii))
    ]
    # rhsr = symmetry_reduce(hsr, tsymop_and_amplitudes)
    # # eigenvalues = eigvals(Hermitian(Matrix(represent(rhsr, hamiltonian))))
    # eigenvalues, _ = eigs(represent(rhsr, hamiltonian); nev=10, ncv=30, which=:SR)
    # append!(evt, real.(eigenvalues))

    psymbed_little = little_symmetry(ssymbed.normal, tii, psymbed)
    for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
        psymop_and_amplitudes = [
            let
                (spin_op[elname] ×ˢ el, ampl)
            end
            for ((el, ampl), elname) in zip(get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic)), element_names(psymbed_little))
        ]
        symop_and_amplitudes = make_product_irrep(tsymop_and_amplitudes, psymop_and_amplitudes)
        rhsr = symmetry_reduce(hsr, symop_and_amplitudes)
        
        # hamiltonian_rep = represent(rhsr, hamiltonian)
        # eigenvalues = eigvals(Hermitian(Matrix(hamiltonian_rep)))
        @info tii, pii, pic, dimension(rhsr)
        eigenvalues, _ = eigs(represent(rhsr, hamiltonian); nev=10, ncv=30, which=:SR)
        append!(evtp, real.(eigenvalues))
        
        # FOO = abs2.(hh - hh')
        # FOO[abs2.(FOO) .< eps(Float64)] .= 0
        # @show FOO
        # break
    
        # @show tii, pii, pic, dimension(rhsr)
        # global totaldim += dimension(rhsr)
    end
end


# for pii in 1:num_irreps(psymbed), pic in 1:irrep_dimension(psymbed, pii)
#     psymop_and_amplitudes = [
#         (spin_op[elname] ×ˢ el, ampl)
#             for ((el, ampl), elname) in zip(get_irrep_iterator(IrrepComponent(psymbed, pii, pic)), element_names(psymbed))
#     ]
#     symop_and_amplitudes = psymop_and_amplitudes

#     @show all([isinvariant(hs, x, hamiltonian) for (x, a) in symop_and_amplitudes])

#     rhsr = symmetry_reduce(hsr, symop_and_amplitudes)
#     @show pii, pic, dimension(rhsr)

#     # hamiltonian_rep = represent(rhsr, hamiltonian)
#     # hh = Matrix(hamiltonian_rep)
#     # @show maximum(abs2.(hh - adjoint(hh) ))

#     # @show imag.(diag(hh))
#     # eigenvalues = eigvals(Hermitian(hh))
#     eigenvalues, _ = eigs(represent(rhsr, hamiltonian); nev=10, ncv=30)
#     append!(evp, real.(eigenvalues))
# end

# sort!(evt)
# sort!(evp)
sort!(evtp)

# H = Matrix(represent( represent(hs) , hamiltonian))
# @show ishermitian(H)
# ev = eigvals(Hermitian(H))
hsr = represent(hs)
@show dimension(hsr)
eigenvalues, _ = eigs(represent(hsr, hamiltonian); nev=10, ncv=30, which=:SR)
ev = eigenvalues

# Plots.plot(size=(100, 800))
# Plots.scatter!(zeros(Float64, 256), ev)
# Plots.scatter!(ones(Float64, 256), evtp)
# Plots.xlims!(-0.5, 1.5)

lr3 = LocalGenPerm([1 => cis(-π/3), 2 => cis( π/3)])

# check Hermiticity

# @show spin_operator(1, :x)
# simplify(symmetry_apply(hs, lr3, spin_operator(1, :x) * (1+0im))) |> prettyprintln
# ishermitian( Matrix(represent(hsr, rotS(5, 1) * rotS(5, 1))) )
# spin_operator(1, :y) |> prettyprintln

op1 = rotS(1, 1) * rotS(2, 1)
op2 = rotS(1, 2) * rotS(2, 2)
op3 = rotS(1, 3) * rotS(2, 3)

# simplify(symmetry_apply(hs, lr3, op1) - op2) |> prettyprintln
# simplify(symmetry_apply(hs, lr3, op1) - op3) |> prettyprintln

using Plots
Plots.plot(size=(400, 800))
Plots.scatter!(zeros(length(evtp)), evtp)
Plots.scatter!(ones(length(ev)), real.(ev))
Plots.xlims!(-0.5, 1.5)
Plots.ylims!(-3.64, -3.60)