using LatticeTools
using QuantumHamiltonian

struct LocalGenPerm{S<:Number} <: AbstractSymmetryOperation
    map::Vector{Pair{Int, S}}

    function LocalGenPerm(map::AbstractVector{<:Pair{<:Integer, S}}) where {S<:Number}
        return new{S}(map)
    end
end


"""
    For permutations: objects on the right (column vectors).
    p3 ≡ p1 * p2
    p3(x) = p1( p2 (x) )

    For generalized permutation, input is (x, ϕ)

    p3(x, ϕ) = p1(p2(x, ϕ))
    p2(x, ϕ) = (p2.map[x], p2.amplitude[x]*ϕ)
    p1(p2(x, ϕ)) = p1( p2.map[x], p2.amplitude[x]*ϕ) = (p1.map[p2.map[x]], p1.amplitude[ p2.map[x] ] * p2.amplitude[x] * ϕ)
"""
function Base.:(*)(p1::LocalGenPerm{S1}, p2::LocalGenPerm{S2}) where {S1, S2}
    S = promote_type(S1, S2)
    return LocalGenPerm([p1.map[i][1] => (p1.map[i][2] * a) for (i, a) in p2.map])
end

function Base.inv(p::LocalGenPerm{S}) where {S}
    newmap = Vector{Pair{Int, S}}(undef, length(p.map))
    for (i, (j, a)) in enumerate(p.map)
        newmap[j] = i => conj(a)
    end
    return LocalGenPerm(newmap)
end


import QuantumHamiltonian.symmetry_apply

function QuantumHamiltonian.symmetry_apply(
    hs::HilbertSpace,
    op::LocalGenPerm{S},
    bitrep::BR,
    bitmask::BR,
) where {S, BR}
    indexarray = extract(hs, bitrep)
    total_ampl = one(S)
    new_indexarray = CartesianIndex([
        let 
            if (get_bitmask(hs, isite) & bitmask) == 0
                istate_new = istate
            else
                istate_new, ampl = op.map[istate]
                total_ampl *= ampl
            end
            istate_new
        end
        for (isite, istate) in enumerate(indexarray.I)
    ]...)
    return (compress(hs, new_indexarray), total_ampl)
end


function QuantumHamiltonian.symmetry_apply(
    hs::HilbertSpace,
    op::LocalGenPerm{S},
    bitrep::BR,
    # bitmask::BR,
) where {S, BR<:Unsigned}
    indexarray = extract(hs, bitrep)
    total_ampl = one(S)
    new_indexarray = CartesianIndex([
        let 
            istate_new, ampl = op.map[istate]
            total_ampl *= ampl
            istate_new
        end
        for (isite, istate) in enumerate(indexarray.I)
    ]...)
    return (compress(hs, new_indexarray), total_ampl)
end



function QuantumHamiltonian.symmetry_apply(
    hs::HilbertSpace,
    symop::LocalGenPerm{S1},
    pop::PureOperator{S2, BR}
) where {S1, S2, BR}
    S = promote_type(S1, S2)
    bm = pop.bitmask # local and does not transform
    br, amr = symmetry_apply(hs, symop, pop.bitrow, pop.bitmask)
    bc, amc = symmetry_apply(hs, inv(symop), pop.bitcol, pop.bitmask) # TODO: THIS NEEDS TO BE INVERSE!
    # @assert br == pop.bitrow # (temporary)!
    # @assert bc == pop.bitcol #(temporary)
    am = pop.amplitude * amr * amc
    return PureOperator{S, BR}(bm, br, bc, am)
end

function QuantumHamiltonian.symmetry_apply(
    hs::HilbertSpace,
    dop::DirectProductOperation,
    obj::PureOperator
)
    return foldr( (symop, obj) -> symmetry_apply(hs, symop, obj), dop.operations; init=obj)
end
