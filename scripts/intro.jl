using DrWatson
@quickactivate "Kitaev"

using Kitaev

using LatticeTools
using QuantumHamiltonian

using PyPlot

using Arpack
using KrylovKit
using DataFrames
using LinearAlgebra


include("localgenperm.jl")

# shape = [3 0; 0 3]
shape = [2 0; 0 2]
# shape = [1 0; 0 1]
# shape = [2 2; -2 4]
# shape = [4 0; 0 4]

honeycomb = make_honeycomb_lattice(shape)

#=
fig = PyPlot.figure()
ax = fig.gca()

colors = ["red", "green", "blue"]
for ((isite, jsite), R, bondtype) in honeycomb.nearest_neighbor_bonds
    ri = fract2carte(honeycomb.lattice.supercell, getsitecoord(honeycomb.lattice.supercell, isite))
    rj = fract2carte(honeycomb.lattice.supercell, getsitecoord(honeycomb.lattice.supercell, jsite) + R)
    ax.plot([ri[1], rj[1]], [ri[2], rj[2]], color=colors[bondtype])
    ax.text((ri[1] + rj[1]) * 0.5, (ri[2] + rj[2]) * 0.5, "$bondtype", color=colors[bondtype])
end
fig.savefig("foo.png")
=#
nsites = sitecount(honeycomb.lattice.supercell)

hs, spin_operator = QuantumHamiltonian.Toolkit.spin_system(sitecount(honeycomb.lattice.supercell), 1//2)

function rotS(isite::Integer, vec::AbstractVector{<:Real})
    @assert length(vec) == 3
    return spin_operator(isite, :x) * vec[1] + spin_operator(isite, :y) * vec[2] + spin_operator(isite, :z) * vec[3]
end

function rotS(isite::Integer, c::Integer)
    rotmat = [ 1/sqrt(6)  -1/sqrt(2)   1/sqrt(3);
               1/sqrt(6)   1/sqrt(2)   1/sqrt(3);
              -2/sqrt(6)   0           1/sqrt(3)]
    return simplify(rotS(isite, rotmat[c, :]))
end


hss = hs
hssr = represent(hss)

all_eigenvalues = []

K = [1, 1, 1]

kitaev0 = simplify(sum(
    rotS(isite, bondtype) * rotS(jsite, bondtype) * K[bondtype]
    for ((isite, jsite), R, bondtype) in honeycomb.nearest_neighbor_bonds
))

xy0 = simplify(sum(
    spin_operator(isite, bondtype) * spin_operator(jsite, bondtype)
    for ((isite, jsite), R, _) in honeycomb.nearest_neighbor_bonds
        for bondtype in [:x, :y]
))

x0 = simplify(sum(
    spin_operator(isite, bondtype) * spin_operator(jsite, bondtype)
    for ((isite, jsite), R, _) in honeycomb.nearest_neighbor_bonds
        for bondtype in [:x,]
))

heisenberg0 = simplify(sum(
    rotS(isite, bondtype) * rotS(jsite, bondtype)
    for ((isite, jsite), R, _) in honeycomb.nearest_neighbor_bonds
        for bondtype in [1,2,3]
))

field0 = simplify(sum(
    spin_operator(isite, :z)
    for isite in 1:sitecount(honeycomb.lattice.supercell)
))

ssymbed = honeycomb.space_symmetry_embedding
tsymbed = ssymbed.normal
psymbed = ssymbed.rest

psym = project(PointSymmetryDatabase.find3d("3"), [1 0 0; 0 1 0])
psymbed = embed(honeycomb.lattice, psym)

@assert isinvariant(hs, tsymbed, kitaev0)
@assert isinvariant(hs, tsymbed, xy0)
@assert isinvariant(hs, tsymbed, field0)
# @assert isinvariant(hs, psymbed, hamiltonian0)
# @assert isinvariant(hs, psymbed, field0)
@show dimension(hssr)

# psym = symmetry(ssymbed.rest)
# @show ssymbed.rest

spin_op = Dict(
    "1" => LocalGenPerm([1=>one(ComplexF64), 2=>one(ComplexF64)]),
    "3<sup>+</sup><sub>001</sub>"  => LocalGenPerm([1=>cis(-π/3), 2=>cis( π/3)]),
    "3<sup>-</sup><sub>001</sub>"  => LocalGenPerm([1=>cis( π/3), 2=>cis(-π/3)]),
    "-1" => LocalGenPerm([1=>one(ComplexF64), 2=>one(ComplexF64)]),
    "-3<sup>+</sup><sub>001</sub>" => LocalGenPerm([1=>cis(-π/3), 2=>cis( π/3)]),
    "-3<sup>-</sup><sub>001</sub>" => LocalGenPerm([1=>cis( π/3), 2=>cis(-π/3)]),
    # "3<sup>+</sup>"  => LocalGenPerm([1=>cis(-π/3), 2=>cis( π/3)]),
    # "3<sup>-</sup>"  => LocalGenPerm([1=>cis( π/3), 2=>cis(-π/3)]),
    # "-1" => LocalGenPerm([1=>one(ComplexF64), 2=>one(ComplexF64)]),
    # "-3<sup>+</sup>" => LocalGenPerm([1=>cis(-π/3), 2=>cis( π/3)]),
    # "-3<sup>-</sup>" => LocalGenPerm([1=>cis( π/3), 2=>cis(-π/3)]),
)

new_elements = [
    let
        els = spin_op[elname]
        e2 = els ×ˢ el
    end
    for (el, elname) in zip(psymbed.elements, psym.element_names)
]

function QuantumHamiltonian.symmetry_apply(
    hs::HilbertSpace,
    symop::AbstractSymmetryOperation,
    op::SumOperator
)
    terms = collect(symmetry_apply(hs, symop, t) for t in op.terms)
    return SumOperator(terms)
end

# totalS1 = sum(spin_operator(i, :x) for i in 1:nsites)
# totalS2 = sum(spin_operator(i, :y) for i in 1:nsites)
# totalS3 = sum(spin_operator(i, :z) for i in 1:nsites)

# @assert all(x -> isinvariant(hs, x, hamiltonian0), new_elements)
# @assert all(x -> isinvariant(hs, x, field0), new_elements)

hsr = represent(hs)
# hamiltonian = xy0
hamiltonian = kitaev0 # - field0
# hamiltonian = field0

evt = Float64[]
evp = Float64[]
evtp = Float64[]

totaldim = 0
for tii in 1:num_irreps(ssymbed.normal)
    tsymop_and_amplitudes = [
        let
            (spin_op["1"] ×ˢ el, ampl)
        end
        for (el, ampl) in get_irrep_iterator(IrrepComponent(ssymbed.normal, tii))
    ]
    rhsr = symmetry_reduce(hsr, tsymop_and_amplitudes)
    eigenvalues = eigvals(Hermitian(Matrix(represent(rhsr, hamiltonian))))
    append!(evt, eigenvalues)
    # collect(get_irrep_iterator(IrrepComponent(ssymbed.normal, tii)))
    # @show tsymop_and_amplitudes

    psymbed_little = little_symmetry(ssymbed.normal, tii, psymbed)
    # new_elements = [
    #     spin_op[elname] ×ˢ el
    #         for (el, elname) in zip(elements(psymbed_little), element_names(psymbed_little))
    # ]
    for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
        psymop_and_amplitudes = [
            let
                (spin_op[elname] ×ˢ el, ampl)
            end
            for ((el, ampl), elname) in zip(get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic)), element_names(psymbed_little))
        ]
        symop_and_amplitudes = make_product_irrep(tsymop_and_amplitudes, psymop_and_amplitudes)
        rhsr = symmetry_reduce(hsr, symop_and_amplitudes)
        hamiltonian_rep = represent(rhsr, hamiltonian)
        eigenvalues = eigvals(Hermitian(Matrix(hamiltonian_rep)))
        append!(evtp, real.(eigenvalues))
        # @show tii, pii, pic, dimension(rhsr)
        # global totaldim += dimension(rhsr)
    end
end


for pii in 1:num_irreps(psymbed), pic in 1:irrep_dimension(psymbed, pii)
    psymop_and_amplitudes = [
        let
            (spin_op[elname] ×ˢ el, ampl)
        end
        for ((el, ampl), elname) in zip(get_irrep_iterator(IrrepComponent(psymbed, pii, pic)), element_names(psymbed))
    ]
    # @show pii, pic, psymop_and_amplitudes
    symop_and_amplitudes = psymop_and_amplitudes

    @show all([isinvariant(hs, x, hamiltonian) for (x, a) in psymop_and_amplitudes])

    rhsr = symmetry_reduce(hsr, symop_and_amplitudes)
    hamiltonian_rep = represent(rhsr, hamiltonian)
    hh = Matrix(hamiltonian_rep)
    @show maximum(abs2.(hh - hh'))
    eigenvalues = eigvals(Hermitian(hh))
    append!(evp, real.(eigenvalues))
    # @show tii, pii, pic, dimension(rhsr)
    # global totaldim += dimension(rhsr)
end


sort!(evt)
sort!(evp)
sort!(evtp)

H = Matrix(represent( represent(hs) , hamiltonian))
@show ishermitian(H)
ev = eigvals(Hermitian(H))
dimension(represent(hs))


# check Hermiticity
# ishermitian( Matrix(represent(hsr, rotS(5, 1) * rotS(5, 1))) )


# r3 = new_elements[2]

# rotS(1,1) |> prettyprintln
# symmetry_apply(hs, r3, rotS(1,1)) |> prettyprintln

# let O = rotS(1,1) + rotS(1,2) + rotS(1,3)
#     simplify(symmetry_apply(hs, lr3, O) - O) |> prettyprintln
# end

# psym = project(PointSymmetryDatabase.find("-3"), [1 0 0; 0 1 0])
# psymbed = embed(psym)

# for tii in 1:num_irreps(ssymbed.normal)
#     @show tii
#     tproj = collect( get_irrep_iterator(IrrepComponent(ssymbed.normal, tii, 1)) )
#     rhssr = symmetry_reduce(hssr, tproj)
#     @show dimension(rhssr)

#     for θ in 0:0.05:1
#         @show θ
#         local hamiltonian = hamiltonian0 * cos(θ * π/2) + field0 * sin(θ * π/2)
#         hamiltonian_rep = represent(rhssr, hamiltonian)
#         # psi = zeros(Float64, dimension(rhssr))
#         # @show hamiltonian_rep * psi
#         # eigenvalues, eigenvectors, info = eigsolve(x -> hamiltonian_rep * x, dimension(rhssr), 10, :SR; krylovdim=20, ishermitian=true)
#         eigenvalues, eigenvectors = eigs(hamiltonian_rep; nev=10, ncv=30, which=:SR)
#         append!(all_eigenvalues, [θ, tii, real(ev), NaN] for ev in eigenvalues)
#     end
# end

# all_eigenvalues = transpose(hcat(all_eigenvalues...))
# df = DataFrame(all_eigenvalues)

# field_values = unique(sort(df[:, :x1]))
# for B in field_values
#     sel = df[:, :x1] .== B
#     evs = df[sel, :x3]
#     min_evs = minimum(evs)
#     df[sel, :x4] = df[sel, :x3] .- min_evs
# end


# @show dimension(rhssr)
# psymbed_little = little_symmetry(ssymbed.normal, tii, ssymbed.rest)

# for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
#     @show pii, pic
#     pproj = collect( get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic)) )

#     # for z2sign in [1, -1]
#     #     z2proj = [(GlobalBitFlip(false), 1), (GlobalBitFlip(true), z2sign)]
#     #     proj = make_product_irrep(z2proj, tproj, pproj)
#     #     rhssr = symmetry_reduce(hssr, proj)
#     #     @show dimension(rhssr)
#     # end
# end