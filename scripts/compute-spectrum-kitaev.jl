using DrWatson
@quickactivate "Kitaev"

using Kitaev

using LatticeTools
using QuantumHamiltonian

using Arpack
using KrylovKit
using ArgParse
using CodecXz
using MsgPack
using Logging
using DataStructures

function compute(shape::AbstractMatrix{<:Integer}, theta_list::AbstractVector{<:Real}, tii_list::AbstractVector{<:Integer},
    nev::Integer,
    ncv::Integer=2*nev+10;
    force::Bool=false,
   )
    shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"
    honeycomb = make_honeycomb_lattice(shape)
    nsites = sitecount(honeycomb.lattice.supercell)
    hs, spin_operator = QuantumHamiltonian.Toolkit.spin_system(sitecount(honeycomb.lattice.supercell), 1//2)
    #=
    function rotS(isite::Integer, vec::AbstractVector{<:Real})
        @assert length(vec) == 3
        return spin_operator(isite, :x)*vec[1] + spin_operator(isite, :y)*vec[2] + spin_operator(isite, :z)*vec[3]
    end
    function rotS(isite::Integer, c::Integer)
        rotmat = [ 1/sqrt(6)   -1/sqrt(6)   1/sqrt(3);
                   1/sqrt(6)    1/sqrt(2)   1/sqrt(3);
                   -sqrt(2/3)   0           1/sqrt(3)]
        return rotS(isite, rotmat[c, :])
    end
    =#
    function S(isite::Integer, c::Integer)
        return spin_operator(isite, [:x, :y, :z][c])
    end

    # for qn_rem in [0, 1, 2, 3]
        # Szmod2 = qn_rem / 2
        # qns = [qn for qn in quantum_number_sectors(hs) if mod(qn[1], 4) == qn_rem]

        # isempty(qns) && continue
        # hss = HilbertSpaceSector(hs, qns)
    let
        hss = hs
        hssr = represent(hss)

        hamiltonian0 = simplify(sum(
            S(isite, bondtype) * S(jsite, bondtype)
            for ((isite, jsite), R, bondtype) in honeycomb.nearest_neighbor_bonds
        ))

        field0 = simplify(sum(
            spin_operator(isite, :x) + spin_operator(isite, :y) + spin_operator(isite, :z)
            for isite in 1:sitecount(honeycomb.lattice.supercell)
        )) * (1/sqrt(3))

        ssymbed = honeycomb.space_symmetry_embedding
        @assert isinvariant(hs, ssymbed.normal, hamiltonian0)
        @assert isinvariant(hs, ssymbed.normal, field0)
        @mylogmsg "Hilbert space dimension: $(dimension(hssr))"
        # for tii in 1:1
        for tii in 1:num_irreps(ssymbed.normal)
            if !isempty(tii_list) && tii ∉ tii_list
                continue
            end
            tproj = collect( get_irrep_iterator(IrrepComponent(ssymbed.normal, tii, 1)) )
            rhssr = symmetry_reduce(hssr, tproj)
            @mylogmsg "reduced Hilbert space dimension: $(dimension(rhssr))"

            for theta in theta_list
                parameters = Dict("shape"=>shape_str, "theta" => theta, "tii" => tii)
                output_filename = savename("spectrum", parameters, "msgpack.xz")
                output_filepath = datadir(output_filename)

                if ispath(output_filepath)
                    if force
                        @mylogmsg "File $output_filepath exists. Overwriting."
                    else
                        @mylogmsg "File $output_filepath exists. Skipping."
                        continue
                    end
                end
                @mylogmsg "Will save to $output_filepath"
                @mylogmsg "theta: $theta"

                hamiltonian = hamiltonian0 * cos(theta * π/2) + field0 * sin(theta * π/2)
                hamiltonian_rep = represent(rhssr, hamiltonian)
                eigenvalues, eigenvectors, info = eigsolve(
                    x -> hamiltonian_rep * x,
                    dimension(rhssr),
                    min(nev, dimension(rhssr)),
                    :SR;
                    krylovdim=min(max(2*nev+1, ncv), dimension(rhssr)),
                    ishermitian=true,
                    verbosity=2
                )

                info = OrderedDict(
                    "converged" => info.converged,
                    # "residual" => info.residual,
                    "normres" => info.normres,
                    "numops" => info.numops,
                    "numiter" => info.numiter,
                )
                save_data = OrderedDict(
                    "parameter" => OrderedDict(
                        "theta" => theta,
                        "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]]
                    ),
                    "sector" => OrderedDict(
                        "tii" => tii,
                        "dimension" => dimension(rhssr),
                    ),
                    "eigenvalue" => eigenvalues,
                    "info" => info,
                )

                @mylogmsg "# Saving to $output_filepath"
                let output_directory = dirname(output_filepath)
                    if !isdir(output_directory)
                        mkpath(output_directory)
                    end
                end
                open(output_filepath, "w") do io
                    ioc = XzCompressorStream(io)
                    MsgPack.pack(ioc, save_data)
                    close(ioc)
                end
            end # for theta
        end # for tii
    end
end



function parse_commandline()
    s = ArgParse.ArgParseSettings()
    ArgParse.@add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--theta"
            arg_type = Float64
            nargs = '*'
            help = "values of theta"
        "--tii"
            arg_type = Int
            nargs = '*'
            help = "tii"
        "--nev"
            arg_type = Int
            default = 100
        "--ncv"
            arg_type = Int
            default = -1
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
        "--debug", "-d"
            help = "debug"
            action = :store_true
    end
    return ArgParse.parse_args(s)
end

function main()

    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])
    theta_list = parsed_args["theta"]
    tii_list = parsed_args["tii"]
    nev = parsed_args["nev"]
    ncv = parsed_args["ncv"]
    force = parsed_args["force"]

    if ncv <= nev
      ncv = 2*nev + 10
    end

    compute(shape, theta_list, tii_list,
        nev, ncv
    ; force=force)
end

main()
